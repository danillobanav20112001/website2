<?php include 'header.php';
include 'vendor/generatePost.php';
?>
<main>
  <div>
    <div class="post">
      <img height="250" src="<?php if (array_key_exists('CONTENT', $_SESSION)) { echo htmlspecialchars($_SESSION['CONTENT']['path']); } else { echo "img/non.jpg"; } ?>" alt="image">
      <div>
        <h3><?php if (array_key_exists('CONTENT', $_SESSION)) { echo $_SESSION['CONTENT']['title']; } else { echo "Пример заголовка"; } ?></h3>
        <h5 class="post-info">Создано <?php if (array_key_exists('CONTENT', $_SESSION)) { echo htmlspecialchars($_SESSION['CONTENT']['date']); } else { echo "Никогда"; } ?>, Автор: <?php if (array_key_exists('CONTENT', $_SESSION)) { echo htmlspecialchars($_SESSION['CONTENT']['author']); } else { echo "Никто"; } ?></h5>
        
          <?php if (array_key_exists('CONTENT', $_SESSION)) { echo $_SESSION['CONTENT']['longText']; } else { echo "Пример Полного описания поста"; } ?>
        
      </div>
    </div>
  </div>
</main>
<?php include 'footer.php'; ?>
