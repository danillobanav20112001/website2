<?php  if(!isset($_SESSION))
    {
        session_start();
    }?>
<!DOCTYPE HTML> 
<html> 
	<head> 
		<link rel="icon" href="img/favicon.ico" type="image/x-icon">
		<link rel="stylesheet" href="style.css">
		<link rel="stylesheet" type="text/css" href="slick/slick.css"/>
		<link rel="stylesheet" type="text/css" href="slick/slick-theme.css"/>
		<title>Данька Лобанов кажется программист</title>
	</head> 
	<body> 
		<header>
			<a href="index.php"><img  src="img/logo.png" width="200" draggable="false" alt="Logo"></a>
			
			<div class="imgUser">
			<div class="blockDataUser">
			<p><?php
			if (array_key_exists('user', $_SESSION)) {
				if($_SESSION['user']){
					echo $_SESSION['user']['full_name'];
				}else{
					echo "Не авторизован";
				}
			}else{
					echo "Не авторизован";
				} ?>
			<br>
			<a href="vendor/logout.php"><?php
			if (array_key_exists('user', $_SESSION)) {
			echo "Выход";
			}else{
			echo "Вход";
			}				
			?>
			</a>
			</p>
			
			</div>
			<div class="blockImgUser">
			<img  src="<?php if (array_key_exists('user', $_SESSION)) {echo $_SESSION['user']['avatar'];}else{echo "img/nonPhoto.png";}	?>"	width="70" draggable="false" alt="imgUser">
			</div>
			</div>
			<nav id="menu1">
				<ul>
					<li><a href="#m1">Новости</a></li>
					<li><a href="#m3">Каталог</a>
						<ul>
							<li><a href="#m3_1">Курсы для детей</a></li>
							<li><a href="#m3_2">Курсы для взрослых</a></li>
							<li><a href="#m3_3">Начинающим специалистам</a></li>
							<li><a href="#m3_4">Проекты qt c++</a></li>
							<li><a href="#m3_5">Индивидуальные курсы</a></li>
						</ul>
					</li>
					<li><a href="#m4">Контакты</a></li>
					<li><a href="<?php if (array_key_exists('user', $_SESSION)){echo "testForm.php";}else{echo "formAutorize.php";}?>">Администрирование</a></li>
				</ul>
			</nav>
		</header>