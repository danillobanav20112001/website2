<?php 
if(!isset($_SESSION))
    {
        session_start();
    }
require_once 'connect.php';

// Выполняем SQL-запрос для получения данных из таблицы CONTENT
$query = "SELECT * FROM CONTENT";
$result = mysqli_query($connect, $query);

if (!$result) {
    die("Ошибка выполнения запроса: " . mysqli_error($connect));
}

// Создаем блоки для каждой строки таблицы
unset($_SESSION['CONTENT']);
while ($row = mysqli_fetch_assoc($result)) {
    $title = $row['title'];
    $shortText = $row['short_text'];
    $textForClickRead = $row['text_for_click_read'];
    $longText = $row['long_text'];
    $author = $row['author'];
    $date = $row['date'];
    $path = $row['image_content'];
	$path2='"' . $path .'"';

    // Выводим информацию в блоке
	
    echo '<div class="post">
			
				<img height="250" src="' .$path . '" alt="Card image cap">
			<div>
				<div>
				<h3>'. $title .'</h3>';
				if(array_key_exists('user', $_SESSION)){
					echo ' 
				<button onclick=editPost('. $path2 .')>Редактировать</button>
				<button onclick=delPost('. $path2 .')>Удалить</button>
				';}
			echo'</div>
				
				<p>
				  ' . $shortText . '
				</p>
				<a href="post.php?param='.$path .'">' . $textForClickRead . '</a>
			  
			  </div>
			  
			</div>';
    
}

// Закрываем соединение с базой данных
mysqli_close($connect);
?>
