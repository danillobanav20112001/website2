<?php
session_start();
require_once 'connect.php';


if(array_key_exists('param', $_GET)){
	$key=$_GET['param'];
	if(!isset($_SESSION))
    {
        session_start();
    }
	require_once 'connect.php';
	$query = "SELECT * FROM CONTENT WHERE image_content='$key'";
	$result = mysqli_query($connect, $query);
	if (!$result) {
    die("Ошибка выполнения запроса: " . mysqli_error($connect));
	}
	$row = mysqli_fetch_assoc($result);
	$_SESSION['CONTENT']=[
	'title' => $row['title'],
    'short_text' => $row['short_text'],
    'textForClickRead' => $row['text_for_click_read'],
    'longText' =>$row['long_text'],
    'author' => $row['author'],
    'date' => $row['date'],
    'path' => $row['image_content'],
		];
	
	// Закрываем соединение с базой данных
	mysqli_close($connect);
	header("Location: ../testForm.php");
}else{
	
	if(array_key_exists('user', $_SESSION)){
		
	// Получение данных из формы и экранирование
	$title = $_POST['title'];
	$short_text = $_POST['short_text'];
	$text_for_click_read = $_POST['text_for_click_read'];
	$long_text = $_POST['long_text'];
	$postOrEdit=$_POST['post_or_edit'];
	$author = $_SESSION['user']['login'];
	$currentDateTime = date("Y-m-d H:i:s");
	
	// Генерация уникального имени файла
	$currentDateTimeForImg = date("Y-m-d_H-i-s");
	$milliseconds = floor((microtime(true) - time()) * 1000);
	$currentDateTimeForImg .= '-' . sprintf('%03d', $milliseconds);

	// Путь к файлу изображения
	$path = '';
	$imgNew=false;
	if (isset($_FILES['image_content']) && $_FILES['image_content']['error'] === UPLOAD_ERR_OK) {
		$imgNew=true;
		$path = 'uploads/' . $_SESSION['user']['password'] . $_SESSION['user']['login'] . '_' . $currentDateTimeForImg . '_' . $_FILES['image_content']['name'];
		move_uploaded_file($_FILES['image_content']['tmp_name'], '../' . $path);
	} else {
		// Если изображение не загружено, используйте "non.jpg"
		$path = 'uploads/' . $_SESSION['user']['password'] . $_SESSION['user']['login'] . '_' . $currentDateTimeForImg . '_' . 'non.jpg';
		// Путь к оригинальному изображению
		$sourceImagePath = '../' . 'img/non.jpg';
		// Путь для сохранения копии изображения
		$destinationImagePath = '../' . $path;

    // Создаем копию изображения
    if (copy($sourceImagePath, $destinationImagePath)) {
        //echo 'Копия изображения успешно создана.';
    } else {
        //echo 'Не удалось создать копию изображения.';
    }
	}

	// Подготовленный запрос для вставки данных в базу данных
	//var_dump($postOrEdit);
	if($postOrEdit=="true"){
		$stmt = mysqli_prepare($connect, "INSERT INTO `content` (`short_text`, `long_text`, `image_content`, `author`, `date`, `title`, `text_for_click_read`) VALUES (?, ?, ?, ?, ?, ?, ?)");
	mysqli_stmt_bind_param($stmt, "sssssss", $short_text, $long_text, $path, $author, $currentDateTime, $title, $text_for_click_read);

	if (mysqli_stmt_execute($stmt)) {
		echo "Статья успешно создана!";
		unset($_SESSION['CONTENT']);	
		// После успешной вставки данных выполните необходимые действия (например, перенаправление на другую страницу)
		// header('Location: success.php');
	} else {
		echo "Произошла ошибка, статья не создана";
		unset($_SESSION['CONTENT']);
		//echo "Произошла ошибка при добавлении данных: " . mysqli_error($connect);
	}

	mysqli_close($connect);
	}else{
		
    // Подготавливаем SQL-запрос с использованием PDO
	
    
    // Заменяем :значение1, :значение2 и условие на фактические значения
    $значение1 = $short_text;
    $значение2 = $long_text;
	if($imgNew){
	$значение3 = $path;
	}
	
    $значение4 = $title;
	$значение5 = $text_for_click_read;
	//$imgNew
	//if (array_key_exists('CONTENT', $_SESSION)) { echo "postData(false)"; } else { echo "postData(true)"; }
	$keyf=$_SESSION['CONTENT']['path'];
    $условие = "image_content='$path'";
    
    $sql;
		if($imgNew){
    $sql = "UPDATE `content` SET short_text = '$значение1', long_text = '$значение2' , image_content = '$значение3', title = '$значение4', text_for_click_read = '$значение5' WHERE image_content = '$keyf'";
}else{
    $sql = "UPDATE `content` SET short_text = '$значение1', long_text = '$значение2' , title = '$значение4', text_for_click_read = '$значение5' WHERE image_content = '$keyf'";
}

    $stmt = mysqli_prepare($connect,$sql);
	if (mysqli_stmt_execute($stmt)) {
		echo "Статья успешно изменена! ";
		unset($_SESSION['CONTENT']);	
		// После успешной вставки данных выполните необходимые действия (например, перенаправление на другую страницу)
		// header('Location: success.php');
	} else {
		echo "Произошла ошибка, статья не изменена";
		unset($_SESSION['CONTENT']);
		//echo "Произошла ошибка при добавлении данных: " . mysqli_error($connect);
	}

	mysqli_close($connect);
	}
	
	
	}else{
	echo "Для создания или редактирования статьи необходимо <a href='formAutorize.php'>авторизоваться</a>!";
	}
	unset($_SESSION['CONTENT']);
}

?>
