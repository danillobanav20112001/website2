<?php  session_start();?>
<?php include 'header.php'; ?>
		<style>
        /* Контейнер, обертывающий изображение и текст */
        .avatar-container {
            position: relative;
            display: inline-block;
        }

        /* Скрыть текст по умолчанию */
        .avatar-text {
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
            background-color: rgba(255, 255, 255, 0.7);
            padding: 5px;
            border: 1px solid #ccc;
            opacity: 0;
            pointer-events: none; /* Чтобы текст не мешал кликам */
            transition: opacity 0.3s ease; /* Плавное появление текста */
        }

        /* Стили для изображения при наведении курсора */
        .avatar-container:hover .avatar-image {
            opacity: 0.7; /* Изменяйте значение opacity по вашему выбору */
        }

        /* Показать текст при наведении курсора на контейнер */
        .avatar-container:hover .avatar-text {
            opacity: 1;
        }
		
.modal {
    display: none;
    position: fixed;
    z-index: 1;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    overflow: auto;
    background-color: rgba(0,0,0,0.7);
  }

  /* Содержимое модального окна */
  .modal-content {
    background-color: #fefefe;
    margin: 15% auto;
    padding: 20px;
    border: 1px solid #888;
    width: 50%;
    text-align: center;
  }

  /* Кнопка для закрытия модального окна */
  .close {
    color: #aaa;
    float: right;
    font-size: 28px;
    font-weight: bold;
  }

  /* Стили для кнопки закрытия при наведении */
  .close:hover {
    color: black;
    cursor: pointer;
  }
    </style>
		<div id="myModal" class="modal">
  <div class="modal-content">
    <span class="close" id="closeModal">&times;</span>
    <p id="modalMessage">Статья создана успешно!</p>
  </div>
</div>


		<div>
		<div class="post">
		<div class="avatar-container">
				<label>
				<img height="250" class="avatar-image" alt="Avatar" src="<?php if (array_key_exists('CONTENT', $_SESSION)) { echo $_SESSION['CONTENT']['path']; } else { echo "img/non.jpg"; } ?>" alt="Card image cap">
				<span class="avatar-text">Выберите картинку</span>
				<!-- Элемент <input> для загрузки файла (скрыт) -->
            <input type="file" id="image_content" name="image_content" style="display: none; "value="img/non.jpg">
				</label>
				</div> 
				<script>
				function showModal(message) {
  var modal = document.getElementById("myModal");
  var modalMessage = document.getElementById("modalMessage");

  modalMessage.innerHTML = message;
  modal.style.display = "block";

  // Закрытие модального окна при клике на "×"
  var closeModalButton = document.getElementById("closeModal");
  closeModalButton.onclick = function() {
    modal.style.display = "none";
  };

  // Закрытие модального окна при клике вне окна
  window.onclick = function(event) {
    if (event.target == modal) {
      modal.style.display = "none";
    }
  };
}

// Функция postData() после успешного создания статьи
function handlePostDataSuccess(response) {
  showModal(response);
}
        // Обработчик изменения элемента <input>
        document.getElementById('image_content').addEventListener('change', function() {
            const fileInput = this;
            const imageElement = document.querySelector('.avatar-image');

            // Проверяем, что был выбран файл
            if (fileInput.files.length > 0) {
                const selectedFile = fileInput.files[0];

                // Проверяем, что выбранный файл - изображение
                if (selectedFile.type.startsWith('image/')) {
                    const reader = new FileReader();

                    // Читаем выбранный файл и устанавливаем его как источник для элемента <img>
                    reader.onload = function(e) {
                        imageElement.src = e.target.result;
                    };

                    reader.readAsDataURL(selectedFile);
                } else {
                    alert('Пожалуйста, выберите изображение.');
                }
            }
        });

        // Обработчик клика на изображение для открытия элемента <input>
         function postData(postOrEdit) {
            var title = document.getElementById("title").innerHTML;
			
            var short_text = document.getElementById("short_text").innerHTML;
			console.log( short_text);
            var text_for_click_read = document.getElementById("text_for_click_read").innerHTML;
            var long_text = document.getElementById("long_text").innerHTML;
            var image_content = document.getElementById("image_content").files[0];
			console.log("Данные: ",title,short_text, long_text, image_content,"------>>>",postOrEdit);
            var formData = new FormData();
            formData.append("title", title);
            formData.append("short_text", short_text);
            formData.append("text_for_click_read", text_for_click_read);
            formData.append("long_text", long_text);
            formData.append("image_content", image_content);
			formData.append("post_or_edit", postOrEdit);
            var xhr = new XMLHttpRequest();
            xhr.open("POST", "vendor/createOrEditPost.php", true);

            xhr.onreadystatechange = function () {
  if (xhr.readyState == 4 && xhr.status == 200) {
    // Обработка успешного ответа от сервера
    var response = xhr.responseText;
    console.log("Ответ сервера: " + response);
    
    // Вызываем функцию для отображения модального окна
    handlePostDataSuccess(response);

    
  }
};


            xhr.send(formData);
			
        }
    </script>
			<div> 
			
				<h3 id="title" contenteditable="true" data-empty="false" style="white-space: pre-wrap;white-space: -moz-pre-wrap;white-space: -pre-wrap;white-space: -o-pre-wrap;word-wrap: break-word;   min-width: 1px;"><?php if (array_key_exists('CONTENT', $_SESSION)) { echo $_SESSION['CONTENT']['title']; } else { echo "Это поле содержит заголовок статьи, кликни и измени его"; } ?></h3>
				
				<div id="short_text" data-empty="false" contenteditable="true" style="white-space: pre-wrap; min-width: 1px;word-wrap: normal;"><?php if (array_key_exists('CONTENT', $_SESSION)) { echo $_SESSION['CONTENT']['short_text']; } else { echo "Чтобы добавить описание статьи, вы можете просто нажать на этот текст и внести свои изменения прямо здесь."; } ?></div>
				<a id="text_for_click_read" contenteditable="true" data-empty="false" style="white-space: pre-wrap; min-width: 1px;word-wrap: normal;"><?php if (array_key_exists('CONTENT', $_SESSION)) { echo $_SESSION['CONTENT']['textForClickRead']; } else { echo "Читать дальше (кликнув сюда, люди перейдут на ваш пост)"; } ?></a>
				<div style="margin: 20px 0;	padding: 0;	height: 0;	border: none;	border-top: 1px solid #333;"></div>
				<div id="long_text"  contenteditable="true" style="white-space: pre-wrap; min-width: 1px;word-wrap: normal;"><?php if (array_key_exists('CONTENT', $_SESSION)) { echo $_SESSION['CONTENT']['longText']; } else { echo "Тут вам необходимо написать всю остальную статью"; } ?></div>
			  </div>
			</div>
		
		
			  </div>
			  <a onclick="<?php if (array_key_exists('CONTENT', $_SESSION)) { echo "postData(false)"; } else { echo "postData(true)"; } ?>" class="button7" style="user-select: none;"><?php if (array_key_exists('CONTENT', $_SESSION)) { echo "Сохранить изменения"; } else { echo "Создать статью"; } ?></a>
		
		<div><br></div>
<?php include 'footer.php'; ?>		

