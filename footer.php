<footer>
		  <div>
			<div>
			  <div>
				<img src="img/logo.png" alt="Logo" width="250">
			  </div>
			  <div class="footer-columns">
				<div class="footer-column">
				  <h3>О сайте</h3>
				  <p>
					Этот сайт создан исключительно, чтобы я сдал предмет по web программированию, он хранит в себе много тайн, которые вы, вероятнее всего, не узнаете
				  </p>
				</div>
				<div class="footer-column">
				  <h3>Мои контакты</h3>
				  <p>
					Если вы хотите написать пожелания или просто пообщаться, то ниже есть моя почта
					<br><strong>Email: <a href="#">indurevol@mail.com</a></strong>
				  </p>
				  
				</div>
			  </div>
			</div>
			<center>
			  <small>Copyright &copy; 2023 Программист Данька Лобанов</small>
			</center>
		  </div>
		</footer>
		
		<script type="text/javascript" src="https://code.jquery.com/jquery-1.11.0.min.js"></script>
		<script type="text/javascript" src="https://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
		<script type="text/javascript" src="slick/slick.min.js"></script>
		<script type="text/javascript" src="js/script.js"></script>
	</body> 
</html>