# Вебсайт с авторизацией и редактированием постов

Это исходный код сайта с домашней работы.
На сайте можно редактировать статьи, создавать статьи, регистрироваться, авторизовываться.


Чтобы сайт работал, необходимо будет создать или перенести БД DBSITE.sql в phpmyadmin или другую СУБД, а так же прописать все ссылки, логины и пароли в файле vendor/connect.php

Пример: host:'localhost',user:'root',password:'',database:'test'

![image info](readmeImage/1.PNG)
![image info](readmeImage/2.PNG)
![image info](readmeImage/3.PNG)
![image info](readmeImage/4.PNG)
![image info](readmeImage/5.PNG)